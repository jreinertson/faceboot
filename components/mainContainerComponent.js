users =  require("json-loader!../src/users.json");

angular.module('app')
.component('mainspot',{
    template: `<div class=\"container\">
                    <div class=\"content\">
                        <postspot></postspot>
                    </div>
                    <div class=\"aside\">
                        <span>Users</span>
                        <usersidebar>
                            <ul>
                            <a ng-repeat="user in users" class=\"profile profile--lg profile--hover-ul\" href=\"/profile/{{user.name}}\">
                                <span>{{ user.name }}</span>
                            </a>
                            </ul>
                        </usersidebar>
                    </div>
                </div>
                <footer class=\"footer\"></footer>`,
    controller: function($scope){
        $scope.users = users;
    }
});