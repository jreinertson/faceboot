posts =  require("json-loader!../src/posts.json");

angular.module('app')
.component('postspot',{
    template: `<div ng-repeat="post in posts" class=\"post\">
                    <div class=\"post__header flex-between margin-16\">
                        <a class=\"profile profile--lg profile--hover-ul\" href=\"/profile/{{post.name}}\">
                            <img class="profilepic">
                            <span>{{ post.name }}</span>
                        </a>
                        <span class=\"timestamp\">{{ post.date }}</span>
                    </div>
                    <div class=\"text-md margin-16\">{{ post.text }}</div>
                    <postFooterSpot></postFooterSpot>
                    <postCommentsSpot></postCommentsSpot>
                </div>`,
    controller: function($scope){
        $scope.posts = posts;
    }
})

.component('postfooterspot',{
    template:   `<div class=\"flex-between margin-16\">
                    <div class=\"flex\">
                        <a ng-click=\"cliccLikk()\" class=\"btn-icon btn--hover-red\" href=\"#\" ng-class="heartClass">
                            <i class=\"fa fa-heart fa-lg\"></i>
                        </a>
                        <span ng-bind="likes"></span>
                    </div>
                    <div class=\"flex\">
                        <a class=\"btn-icon\" href=\"#\">
                            <i class=\"fa fa-comments-o fa-lg\"></i>
                        </a>
                        <span ng-bind="comments.length"></span>
                    </div>
                </div>`,
    controller: function($scope){
        $scope.likes = $scope.$parent.post.likes;
        $scope.comments = $scope.$parent.post.comments;
        var liked = false;
        $scope.cliccLikk = function(){
            if(!liked){
                $scope.likes++;
                liked = true;
                $scope.heartClass = "btn--active";
            }
            else{
                $scope.likes--;
                liked = false;
                $scope.heartClass = "";
            }
        }
    }
})

.component('postcommentsspot',{
    template: `<ul class=\"margin-16 top-divider\">
                    <div class=\"comments\" ng-repeat="comment in comments">
                        <comment>
                            <li class=\"flex-col\"><div class=\"flex-between\"><a class=\"profile profile--hover-ul\" href=\"/profile/{{comment.name}}\"><img class="profilepic"><span>{{comment.name}} </span></a><span class=\"timestamp text-sm\">{{comment.date}} </span></div><p class=\"text-md margin-4-top\"> {{comment.text}} </p></li>
                        </comment>
                    </div>
                    <div class=\"p-input\"><input class=\"p-input__ta\" placeholder=\"Add a comment...\" ng-model=\"commentfield\"></div><a class=\"btn btn-primary f-right margin-8-v\" href=\"#\" ng-click=\"clicc()\">please clicc me 😊</a></div>
                </ul>`,
    controller: function($scope){
        $scope.comments = $scope.$parent.post.comments;
        $scope.clicc = function(){
            $scope.comments.push({name:"Jacob", text:$scope.commentfield, date: new Date()});
            $scope.commentfield = '';
        }
    }
})
