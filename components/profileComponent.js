users =  require("json-loader!../src/users.json");

angular.module('app')
.config(function($stateProvider){
        $stateProvider.state('state2',{
        url: '/profile/:userName',
        component: 'another'
    })
})
.component('another',{
    template: `<div>
                <a class=\"profile profile--lg profile--hover-ul\" href=\"/\">
                    <h1>Back</h1>
                </a>
                <ul>
                    <h1>Name: {{$ctrl.username}}</h1>
                    <h1>Age: {{$ctrl.age}}</h1>
                    <h1>Location: {{$ctrl.location}}</h1>
                    <h1>Bio: {{$ctrl.bio}}</h1>
                </ul>
               </div>
               `,
    controller: function($stateParams) {
        users.forEach(function(element) {
            if (element.name == $stateParams.userName){
                this.location = element.location;
                this.age = element.age;
                this.bio = element.bio;
                this.img = element.picture;
            }
        }, this);
        this.username = $stateParams.userName;
    }
})