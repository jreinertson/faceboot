angular.module('app')

.config(function($stateProvider){
    $stateProvider.state('root',{
        url: '/',
        component: 'root'
    })
})
.component('root',{
    template: '<body><headerspot></headerspot><mainspot></mainspot></body>'
});