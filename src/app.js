require('./assets/styles/index.css');

/**
 * Insert your code here !
 */
require('angular');
require('angular-ui-router');

angular.module('app',['ui.router'])

.config(function($stateProvider, $locationProvider,$urlRouterProvider){
    $locationProvider.html5Mode(true);
    $urlRouterProvider.otherwise('/');
})

.run(function(){
        console.log("module is alive")
});

require("../components/rootComponent.js");
require("../components/headerComponent.js");
require("../components/mainContainerComponent.js");
require("../components/postComponent.js");
require("../components/profileComponent.js");


console.dir(angular);