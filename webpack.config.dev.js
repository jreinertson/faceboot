const path = require('path');
//const CopyWebpackPlugin = require('copy-webpack-plugin');
module.exports = {
	devtool: 'inline-source-map',
	entry: [
		path.resolve(__dirname, 'src/app.js')
	],
	target: 'web',
	output: {
		path: path.resolve(__dirname, 'src'),
		publicPath: '/',
		filename: 'bundle.js'
	},
	plugins: [],
	module: {
		rules: [
			{test: /\.(png|jpg)$/,loader:'url-loader'},
			{test: /\.css$/, loaders: ['style-loader','css-loader']}
		]
	}
}